#!/usr/bin/env python
# -*-coding:Utf-8 -*

import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(name='DeclaTravaux',
    version='0.1',
    description='Utilitaire de transmission de déclarations issues de plateforme http://www.reseaux-et-canalisations.ineris.fr/',
    long_description=read('README.rst'),
    author='Pierre Gobin',
    author_email='contact@pierregobin.fr',
    url='https://framagit.org/Pierre86/DeclaTravaux',
    license='CeCILL',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Office/Business',
        'License :: OSI Approved :: CEA CNRS Inria Logiciel Libre License, version 2.1 (CeCILL-2.1)',
        'Programming Language :: Python :: 3',
    ],
    packages=find_packages(),
    install_requires=[
        'asn1crypto>=0.23.0',
        'cryptography>=2.0.3',
        'dbus-python>=1.2.4',
        'idna>=2.6',
        'keyring>=10.4.0',
        'pycparser>=2.18',
        'PyPDF2>=1.26.0',
        'SecretStorage>=2.3.1',
        'six>=1.11.0',
    ],
    python_requires='>=3',
    entry_points={
        'console_scripts': [
            'DeclaTravaux=DeclaTravaux',
        ],
    },
)
