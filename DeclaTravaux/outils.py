#!/usr/bin/env python
# -*-coding:Utf-8 -*

import os
import re

from zipfile import ZipFile

from DeclaTravaux.traitement_PDF import getIntituleDeclaration

def rechercherArchives(repertoire, regexArchive):
    """
    Recherche, dans le répertoire indiqué en premier paramètre,
    les fichiers .zip dont les noms correspondent à l'expression régulière donnée en second paramètre.
    """
    # On se place dans le dossier donné en paramètre
    os.chdir(repertoire)
    # On liste les fichiers contenus dans le dossier
    listeFichiers = os.listdir()
    # On filtre la liste pour n'obtenir que les fichiers correspondant à l'expression régulière
    listeArchives = [fichier for fichier in listeFichiers if re.match(regexArchive, fichier)]

    return listeArchives


def trierArguments(arguments, regexArchive):
    """
    Parmi la liste des arguments donnés en premier paramètre,
    renvoie ceux qui répondent à l'expression régulière donnée en second paramètre.
    """

    for fichier in arguments:

        cheminArchiveTestee = os.path.abspath(fichier)
        nomArchiveTestee = os.path.basename(cheminArchiveTestee)

        # Si l'argument donné correspond à une archive existante et qu'elle est au format .zip attendu,
        # elle est ajoutée à la liste des archives

        if os.path.isfile(cheminArchiveTestee) and re.match(regexArchive, nomArchiveTestee):
            listeArchives.append(cheminArchiveTestee)

    return listeArchives


def choisirArchive(listeArchives):
    """
    Propose les différentes archives à l'utilisateur.
    Renvoie l'archive choisie par l'utilisateur.
    """

    # On trie la liste par ordre décroissant
    listeArchives.sort(reverse=True)

    # La liste des archives est affichée
    # N : nomArchive
    for rang, archive in enumerate(listeArchives):

        numeroDeclaration = os.path.splitext(os.path.basename(archive))[0]
        intituleDeclaration = getIntituleDeclaration(numeroDeclaration)

        print(rang, ':', intituleDeclaration)

    choix = -1

    # On propose à l'utilisateur de choisir l'archive qu'il désire traiter.
    # La demande boucle tant que la saisie n'est pas valide (entrée qui n'est pas un nombre ou nombre hors index).
    while choix < 0 or choix >= len(listeArchives):

        saisie = input('Sélectionner l\'archive que vous souhaitez extraire : ')

        try:
            choix = int(saisie)
            assert choix >= 0 and choix < len(listeArchives)
        except (ValueError, AssertionError):
            print('Vous devez saisir un nombre compris entre 0 et ' + str(len(listeArchives)-1) + '.')
            choix = -1

    return listeArchives[choix]


def validerArchive(numeroDeclaration):
    """
    Valide le choix de l'archive saisie et renvoyant False ou True.
    """

    choix = False

    print('La déclaration n° ' + numeroDeclaration + ' a été sélectionnée.')

    while choix is False:

        saisie = input('Continuer ? Oui / Non : ')

        if saisie.lower() == 'oui' or saisie.lower() == 'o':
            validation = True
            choix = True
        elif saisie.lower() == 'non' or saisie.lower() == 'n':
            validation = False
            choix = True
        else:
            choix = False

    return validation


def extraireArchive(cheminArchive, numeroDeclaration, repertoireDeclaration=''):
    """
    Extrait l'archive obtenue après l'utilisation du téléservice dans le répertoire indiqué en paramètre.
    Retourne le chemin vers le dossier où l'archive a été extraite.
    """

    # Création d'un répertoire qui contiendra le contenu extrait de l'archive ;
    # ce répertoire a pour nom le numéro de la déclaration.
    destination = os.path.join(repertoireDeclaration, numeroDeclaration)
    os.makedirs(destination)

    # On extrait l'ensemble du contenu de l'archive
    # dans le répertoire venant d'être créé.
    archive = ZipFile(cheminArchive)
    archive.extractall(destination)
    archive.close()

    # L'archive principale contient elle-même une archive
    # On extrait donc cette sous-archive et on supprime le fichier .zip correspondant

    cheminSousArchive = os.path.join(destination, numeroDeclaration + '_description.zip')

    sousArchive = ZipFile(cheminSousArchive)
    sousArchive.extractall(destination)
    sousArchive.close()
    os.remove(cheminSousArchive)

    # L'archive est déplacée dans le dossier « Archives » du répertoire des déclarations
    os.renames(cheminArchive, os.path.join(repertoireDeclaration, 'Archives', os.path.basename(cheminArchive)))

    return True
