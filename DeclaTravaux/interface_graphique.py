#!/usr/bin/env python
# -*-coding:Utf-8 -*

# Chargement des bibliothèques standards
import locale
import os
import re
import smtplib
import sys
import socket

from tkinter import *
from tkinter.ttk import *
from tkinter import font
import tkinter.filedialog as tkFileDialog

# Chargement des bibliothèques locales
from DeclaTravaux.configuration import getParametres
from DeclaTravaux.configuration import setParametres
from DeclaTravaux.configuration import getMotDePasse
from DeclaTravaux.configuration import setMotDePasse

from DeclaTravaux.courriels import envoyerCourriel
from DeclaTravaux.courriels import envoyerCourrielRecap

from DeclaTravaux.traitement_PDF import getListeDestinataires
from DeclaTravaux.traitement_PDF import getIntituleDeclaration

from DeclaTravaux.outils import rechercherArchives
from DeclaTravaux.outils import extraireArchive
from DeclaTravaux.outils import choisirArchive
from DeclaTravaux.outils import trierArguments

locale.setlocale(locale.LC_ALL, "")


# Définition des variables globales

PREMIER_LANCEMENT = getParametres()['Configuration'].getboolean('PremierLancement')

REPERTOIRE_RECHERCHE = getParametres()['Configuration']['RepertoireRecherche']
REPERTOIRE_DECLARATIONS = getParametres()['Configuration']['RepertoireDeclarations']

ADRESSE_ELECTRONIQUE = getParametres()['Courriels']['AdresseElectronique']
SERVEUR_SMTP = getParametres()['Courriels']['ServeurSMTP']
PORT_SERVEUR_SMTP = getParametres()['Courriels']['PortServeurSMTP']
NOM_EXPEDITEUR = getParametres()['Courriels']['NomExpediteur']

def selectionArchive(application, cheminArchive=''):

    # === I. SÉLECTION DE L'ARCHIVE ===

    #On définit le format de fichier .zip attendu
    regexArchive = re.compile(r'[0-9]{13}[A-Z]{1}([0-9][0-9])?_(DT|DICT|DDC|ATU).zip')
    listeArchives = list()

    # Si aucun argument n'a été donné lors de l'appel du programme,
    # on retourne la liste des archives trouvées
    # dans le dossier « Téléchargements ».
    if len(sys.argv) < 2:
        listeArchives = rechercherArchives(REPERTOIRE_RECHERCHE, regexArchive)

    # À l'inverse, si des arguments ont été donnés lors de l'appel du programme,
    # on retourne la liste des archives trouvées parmi les arguments donnés.
    else:
        listeArchives = trierArguments(sys.argv[1:])


    # Si la liste est vide, on propose à l'utilisateur de sélectionner une archive
    if not listeArchives:
        application.etape = 'aucuneArchive'
        #tkMsgBox.showinfo('Déclaration de travaux', 'Aucune déclaration trouvée.')
        #sys.exit()

    # Si la liste contient un seul chemin d'archive, ce chemin est affecté à la variable archive
    elif len(listeArchives) == 1:

        application.archive = listeArchives[0]
        application.numeroDeclaration = os.path.splitext(os.path.basename(application.archive))[0]
        application.intituleDeclaration = getIntituleDeclaration(application.numeroDeclaration)
        application.etape = 'confirmationArchive'

    # Enfin, si la liste contient plusieurs archives,
    # on demande à l'utilisateur l'archive qu'il souhaite traiter.
    else:

        application.listeArchives = listeArchives
        application.etape = 'choixArchive'


def confirmerArchive(application, selection):
    application.archive = application.listeArchives[selection]
    application.numeroDeclaration = os.path.splitext(os.path.basename(application.archive))[0]
    application.intituleDeclaration = getIntituleDeclaration(application.numeroDeclaration)
    application.etape = 'confirmationArchive'

def confirmerArchivePerso(application, archive):
    application.archive = archive
    application.numeroDeclaration = os.path.splitext(os.path.basename(application.archive))[0]
    application.intituleDeclaration = getIntituleDeclaration(application.numeroDeclaration)
    application.etape = 'confirmationArchive'


def transmettreDeclaration(application):

    application.etape = 'transmissionDeclaration'

    # === II. TRAITEMENT DE L'ARCHIVE ===

    # L'archive sélectionnée est extraite dans un répertoire nommé selon le numéro de la déclaration
    application.etatTransmission = 'Extraction de l\'archive'
    extraireArchive(application.archive, application.numeroDeclaration, REPERTOIRE_DECLARATIONS)

    # On récupère la liste des destinataires
    application.etatTransmission = 'Identification des destinataires'
    listeDestinataires = getListeDestinataires(application.numeroDeclaration)


    # === III. ENVOI DES COURRIELS ===

    # Paramètres d'identification de l'expéditeur et connexion au serveur SMTP
    application.etatTransmission = 'Connexion au serveur SMTP'
    application.expediteur = ADRESSE_ELECTRONIQUE

    try:
        serveur = smtplib.SMTP_SSL(SERVEUR_SMTP, PORT_SERVEUR_SMTP)
        serveur.login(ADRESSE_ELECTRONIQUE, getMotDePasse())

        # Envoi du courriel à chaque destinataire
        application.etatTransmission = 'Envoi des courriels'
        listeDesEnvois = list()

        for destinataire in listeDestinataires:
            envoi = envoyerCourriel(serveur, application.expediteur, NOM_EXPEDITEUR, destinataire, REPERTOIRE_DECLARATIONS, application.numeroDeclaration)
            listeDesEnvois.append(envoi)

        # Envoi du courriel récapitulatif
        application.etatTransmission = 'Envoi du courriel récapitulatif'
        envoyerCourrielRecap(serveur, application.expediteur, application.expediteur, application.numeroDeclaration, listeDesEnvois)

        # Fermeture de la connexion SMTP
        serveur.quit()


    except socket.gaierror as erreur:
        # En cas d'erreur liée au réseau

        # L'archive est rétablie dans son dossier initial
        os.renames(os.path.join(REPERTOIRE_DECLARATIONS, 'Archives', application.archive), os.path.join(REPERTOIRE_RECHERCHE, application.archive))

        # Le dossier de travail correspondant à la déclaration en cours est vidé puis supprimé
        for fichier in os.listdir(os.path.join(REPERTOIRE_DECLARATIONS, application.numeroDeclaration)):
            os.remove(os.path.join(REPERTOIRE_DECLARATIONS, application.numeroDeclaration, fichier))

        os.rmdir(os.path.join(REPERTOIRE_DECLARATIONS, application.numeroDeclaration))

        application.etape = 'erreurTransmission'

    application.etatTransmission = 'Terminée'
    application.etape = 'finTransmission'




class FenParametres(Toplevel):

    def __init__(self, parent):
        """Constructeur de la fenêtre Préférences"""
        Toplevel.__init__(self)

        # Définition de la largeur des champs
        largEntree = 30

        # Affectation du titre de la fenêtre - Fenêtre non redimensionnable
        self.title('DéclaTravaux - Déclaration de travaux')
        self.resizable(width=False, height=False)

        # Titre « Paramètres » de la fenêtre
        Label(self, text='Paramètres', font=font.Font(size=11, weight='bold')).grid(row=0, column=0, columnspan=2, padx=20, pady=10)

        # Premier groupe de champs : paramètres de configuration générale
        groupeConfig = LabelFrame(self, text='Configuration')

        Label(groupeConfig, text='Répertoire de recherche :').grid(row=0, column=0, padx=10, pady=5, sticky=W)
        self.repertoireRecherche = Entry(groupeConfig, width=largEntree)
        self.repertoireRecherche.insert(0, REPERTOIRE_RECHERCHE)
        self.repertoireRecherche.grid(row=0,column=1, padx=10, pady=5)

        Label(groupeConfig, text='Répertoire de traitement :').grid(row=1, column=0, padx=10, pady=5, sticky=W)
        self.repertoireDeclarations = Entry(groupeConfig, width=largEntree)
        self.repertoireDeclarations.insert(0, REPERTOIRE_DECLARATIONS)
        self.repertoireDeclarations.grid(row=1,column=1, padx=10, pady=5)

        groupeConfig.grid(row=1, column=0, columnspan=2, padx=10, pady=10)

        # Deuxième groupe de champs : paramètres liés à l'envoi des courriels
        groupeCourriel = LabelFrame(self, text='Courriels')

        Label(groupeCourriel, text='Adresse électronique :').grid(row=0, column=0, padx=10, pady=5, sticky=W)
        self.adresseElectronique = Entry(groupeCourriel, width=largEntree)
        self.adresseElectronique.insert(0, ADRESSE_ELECTRONIQUE)
        self.adresseElectronique.grid(row=0,column=1, padx=10, pady=5)

        self.btnModifierMDP = Button(groupeCourriel, text='Modifier le mot de passe', command=self.afficherMDP)
        self.etiMDP = Label(groupeCourriel, text='Mot de passe :')
        self.motDePasse = Entry(groupeCourriel, show='●', width=largEntree)

        if (PREMIER_LANCEMENT == 1):
            self.etiMDP.grid(row=1, column=0, padx=10, pady=5, sticky=W)
            self.motDePasse.grid(row=1,column=1, padx=10, pady=5)
        else:
            self.btnModifierMDP.grid(row=1, column=0, columnspan=2, padx=10, pady=5, sticky=EW)

        Label(groupeCourriel, text='Adresse du serveur SMTP :').grid(row=2, column=0, padx=10, pady=5, sticky=W)
        self.serveurSMTP = Entry(groupeCourriel, width=largEntree)
        self.serveurSMTP.insert(0, SERVEUR_SMTP)
        self.serveurSMTP.grid(row=2,column=1, padx=10, pady=5)

        Label(groupeCourriel, text='Port du serveur SMTP :').grid(row=3, column=0, padx=10, pady=5, sticky=W)
        self.portServeurSMTP = Entry(groupeCourriel, width=largEntree)
        self.portServeurSMTP.insert(0, PORT_SERVEUR_SMTP)
        self.portServeurSMTP.grid(row=3,column=1, padx=10, pady=5)

        Label(groupeCourriel, text='Nom de l\'expéditeur :').grid(row=4, column=0, padx=10, pady=5, sticky=W)
        self.nomExpediteur = Entry(groupeCourriel, width=largEntree)
        self.nomExpediteur.insert(0, NOM_EXPEDITEUR)
        self.nomExpediteur.grid(row=4,column=1, padx=10, pady=5)

        groupeCourriel.grid(row=2, column=0, columnspan=2, padx=10, pady=10)

        # Création des boutons « Quitter » et « Valider »
        Button(self, text='Quitter', command=self.destroy).grid(row=5, column=0, pady=10)
        Button(self, text='Valider', command=self.validerParametres).grid(row=5, column=1, pady=10)

        # Positionnement de la fenêtre et raffraichissement de l'affichage
        self.centrerFenetre()

    def afficherMDP(self):
        """ Fonction affichant l'étiquette et le champ « Mot de passe » """

        # Le bouton « Modifier le mot de passe est masqué »
        self.btnModifierMDP.grid_remove()

        # L'étiquette et le champ « Mot de passe » sont affichés à la place
        self.etiMDP.grid(row=1, column=0, padx=10, pady=5, sticky=W)
        self.motDePasse.insert(0, getMotDePasse())
        self.motDePasse.grid(row=1,column=1, padx=10, pady=5)

    def validerParametres(self):
        """ Fonction enregistrant les paramètres entrés """

        # Mise à jour du fichier de configuration
        setParametres(  RepertoireRecherche=self.repertoireRecherche.get(),
                        RepertoireDeclarations=self.repertoireDeclarations.get(),
                        AdresseElectronique=self.adresseElectronique.get(),
                        ServeurSMTP=self.serveurSMTP.get(),
                        PortServeurSMTP=self.portServeurSMTP.get(),
                        NomExpediteur=self.nomExpediteur.get())

        # Les variables globales sont également modifiées, pour que les changements
        # soient pris en compte immédiatement
        global PREMIER_LANCEMENT
        PREMIER_LANCEMENT = getParametres()['Configuration'].getboolean('PremierLancement')
        global REPERTOIRE_RECHERCHE
        REPERTOIRE_RECHERCHE = getParametres()['Configuration']['RepertoireRecherche']
        global REPERTOIRE_DECLARATIONS
        REPERTOIRE_DECLARATIONS = getParametres()['Configuration']['RepertoireDeclarations']
        global ADRESSE_ELECTRONIQUE
        ADRESSE_ELECTRONIQUE= getParametres()['Courriels']['AdresseElectronique']
        global SERVEUR_SMTP
        SERVEUR_SMTP = getParametres()['Courriels']['ServeurSMTP']
        global PORT_SERVEUR_SMTP
        PORT_SERVEUR_SMTP = getParametres()['Courriels']['PortServeurSMTP']
        global NOM_EXPEDITEUR
        NOM_EXPEDITEUR = getParametres()['Courriels']['NomExpediteur']

        # Si le champ « Mot de passe » est visible, cela signifie qu'il a été modifié
        # Il faut donc modifier le mot de passe enregistré
        if (self.motDePasse.winfo_ismapped()):
            setMotDePasse(self.motDePasse.get())

        # On s'assure que le bouton 3 est activé dans la mesure où les paramètres
        # ont été enregistrés au moins une fois
        self.master.btn3['state'] = NORMAL
        self.destroy()

    def centrerFenetre(self):
        """Méthode de positionnement de la fenêtre pour qu'elle soit centrée à l'écran"""

        self.update_idletasks()

        largeurFenetre, hauteurFenetre = self.winfo_reqwidth(), self.winfo_reqheight()
        largeurEcran, hauteurEcran = self.winfo_screenwidth(), self.winfo_screenheight()

        positionX = (largeurEcran - largeurFenetre) // 2
        positionY = (hauteurEcran - hauteurFenetre) // 2

        self.geometry("%dx%d%+d%+d" % (largeurFenetre, hauteurFenetre, positionX, positionY))


class FenPrincipale(Tk):

    def __init__(self):
        """Constructeur de la fenêtre principale"""
        Tk.__init__(self)

        # Définition des propriétés
        self._etape = ''
        self._etatTransmission = 'Initialisation'

        self.listeArchives = list()
        self.archive = ''
        self.numeroDeclaration = ''
        self.intituleDeclaration = ''
        self.expediteur = ''

        # Affectation du titre de la fenêtre - Fenêtre non redimensionnable
        self.title('DéclaTravaux')
        self.resizable(width=False, height=False)

        # La fenêtre est composée de 3 lignes de texte
        self.police1 = font.Font(size=12, weight='bold', slant='roman')
        self.valTexte1 = StringVar()
        self.texte1 = Label(self, textvariable=self.valTexte1, font=self.police1, justify=CENTER)

        self.police2 = font.Font(size=10, weight='normal', slant='roman')
        self.valTexte2 = StringVar()
        self.texte2 = Label(self, textvariable=self.valTexte2, font=self.police2, justify=CENTER)

        self.police3 = font.Font(size=10, weight='normal', slant='italic')
        self.valTexte3 = StringVar()
        self.texte3 = Label(self, textvariable=self.valTexte3, foreground='Blue', font=self.police3, justify=CENTER)

        # Liste (utile si plusieurs archives sont détectées)
        self.liste = Listbox(self, width=40, height=len(self.listeArchives))
        self.entreeMax = 0

        # Champ et bouton (utile si aucune archive n'a été détectée automatiquement
        self.champArchive = Entry(self)
        self.btnSelection = Button(self, text='Chercher')

        # La fenêtre est composée de 3 boutons
        self.valBtn1 = StringVar()
        self.btn1 = Button(self, textvariable=self.valBtn1)

        self.valBtn2 = StringVar()
        self.btn2 = Button(self, textvariable=self.valBtn2)

        self.valBtn3 = StringVar()
        self.btn3 = Button(self, textvariable=self.valBtn3)


    def _get_etape(self):
        return self._etape

    def _set_etape(self, nouvelleEtape):

        # À chaque modification de la propriété étape, l'agencement de la fenêtre est modifié

        if nouvelleEtape == 'demarrage':

            # Le bouton 3 est désactivé s'il s'agit du premier lancement du logiciel
            # pour obliger à modifier les paramètres
            if (PREMIER_LANCEMENT == False):
                etatBtn3 = NORMAL
            else:
                etatBtn3 = DISABLED

            self.valTexte1.set('DéclaTravaux')
            self.texte1.grid(row=0, column=0, columnspan=10, padx=20, pady=15)

            self.valTexte2.set('Utilitaire de transmission des DICT, DT et ATU')
            self.texte2.grid(row=5, column=0, columnspan=10, padx=20, pady=(0,15))

            self.texte3.grid_remove()

            self.valBtn1.set('Quitter')
            self.btn1['command'] = self.quit
            self.btn1.grid(row=20, column=0, padx=(25, 10), pady=10)

            self.valBtn2.set('Paramètres')
            self.btn2['command'] = self.afficherParametres
            self.btn2.grid(row=20, column=1, padx=(25, 10), pady=10)

            self.valBtn3.set('Démarrer')
            self.btn3['state'] = etatBtn3
            self.btn3['command'] = lambda: selectionArchive(self)
            self.btn3.grid(row=20, column=9, padx=(25, 10), pady=10)

            self.centrerFenetre()


        elif nouvelleEtape == 'choixArchive':
            self.valTexte1.set('Choix de la déclaration :')
            self.texte2.grid_remove()
            self.texte3.grid_remove()


            # Création de la liste avec les différentes archives trouvées
            for i, entree in enumerate(self.listeArchives):
                numeroDeclaration = os.path.splitext(os.path.basename(entree))[0]
                intituleDeclaration = getIntituleDeclaration(numeroDeclaration)
                self.liste.insert(i, intituleDeclaration)

                # La largeur de la liste est ajustée selon la longueur de l'entrée la plus longue
                if len(intituleDeclaration) > self.entreeMax:
                    self.entreeMax = len(intituleDeclaration)

            self.liste.configure(width=self.entreeMax)
            self.liste.bind('<<ListboxSelect>>', self.activerBtn3)
            self.liste.grid(row=15, column=0, columnspan=10, padx=25, pady=(0,20))

            self.btn2.grid_remove()

            # Par défaut, le bouton 3 est désactivé
            # Il est activé lorsque une entrée de la liste est sélectionnée
            self.valBtn3.set('Sélectionner')
            self.btn3['state'] = DISABLED
            self.btn3['command'] = lambda: confirmerArchive(self, self.liste.curselection()[0])

            self.centrerFenetre()

        elif nouvelleEtape == 'aucuneArchive':
            self.valTexte1.set('Aucune déclaration trouvée. Veuillez sélectionner une archive :')
            self.texte1.grid(column=0, columnspan=10)
            self.texte2.grid_remove()
            self.texte3.grid_remove()

            self.champArchive.grid(row=17, column=0, columnspan=9, padx=(50,0), pady=(0,10), sticky='ewns')
            self.btnSelection.grid(row=17, column=9, padx=(0,50), pady=(0,10), sticky='ew')
            self.btnSelection['command'] = lambda: self.selectionnerArchive()

            self.btn2.grid_remove()

            # Par défaut, le bouton 3 est désactivé
            # Il est activé lorsque une entrée de la liste est sélectionnée
            self.valBtn3.set('Sélectionner')
            self.btn3['state'] = DISABLED
            self.btn3['command'] = lambda: confirmerArchivePerso(self, self.champArchive.get())
            self.btn3.grid(column=9)

            self.centrerFenetre()

        elif nouvelleEtape == 'confirmationArchive':

            # Texte 1 = La déclaration XXX a été sélectionnée
            # Texte 2 = Souhaitez-vous procéder à la transmission électronique de celle-ci ?
            if self.intituleDeclaration[0] == 'A':
                determinant = 'L’'
            else:
                determinant = 'La '

            self.valTexte1.set(determinant + self.intituleDeclaration + ' a été sélectionnée.')

            self.valTexte2.set('Souhaitez-vous procéder à la transmission électronique de celle-ci ?')
            self.texte2.grid(row=5, column=0, columnspan=10, padx=15, pady=(0,15))

            self.texte3.grid_remove()

            self.liste.grid_remove()
            self.champArchive.grid_remove()
            self.btnSelection.grid_remove()

            self.btn2.grid_remove()

            self.valBtn3.set('Transmettre')
            self.btn3['state'] = ACTIVE
            self.btn3['command'] = lambda: transmettreDeclaration(self)

            self.centrerFenetre()

        elif nouvelleEtape == 'transmissionDeclaration':

            # Lors de la transmission de la déclaration,
            # le texte 3 est mis à jour selon l'avancement
            self.valTexte1.set(self.intituleDeclaration)
            self.valTexte2.set('État de la transmission :')
            self.valTexte3.set(self.etatTransmission + '...')
            self.texte3.grid(row=10, column=0, columnspan=10, padx=15, pady=(0,15))

            self.btn1.grid_remove()
            self.btn2.grid_remove()
            self.btn3.grid_remove()

            self.centrerFenetre()

        elif nouvelleEtape == 'erreurTransmission':

            # Affiché en cas d'erreur rencontrée lors de la transmission
            self.valTexte1.set('Impossible de se connecter au serveur de messagerie.\n\nLa ' + self.intituleDeclaration + ' n\'a pas été transmise.\n\nVérifiez votre connexion Internet.')
            self.police1['size'] = 10
            self.police1['weight'] = 'normal'

            self.valTexte2.set('')
            self.valTexte3.set('')

            self.texte3.grid(row=10, column=0, columnspan=10, padx=15, pady=(0,15))

            self.texte2.grid_remove()
            self.texte3.grid_remove()

            self.btn2.grid()

            self.btn1.grid_remove()
            self.btn3.grid_remove()

            self.centrerFenetre()

        elif nouvelleEtape == 'finTransmission':

            # Confirmation de la transmission de la déclaration
            self.valTexte2.set('La transmission électronique de la déclaration est terminée.')
            self.police3['slant'] = 'roman'
            self.valTexte3.set('Un courriel récapitulatif a été envoyé à l\'adresse ' + self.expediteur + '.')

            self.valBtn2.set('OK')
            self.btn2['command'] = self.quit
            self.btn2['default'] = ACTIVE
            self.btn2.grid(column=0, columnspan=10)

            self.centrerFenetre()

        else:
            print('Étape inconnue :', nouvelleEtape)
            nouvelleEtape = self._etape

        self._etape = nouvelleEtape

    def _get_etatTransmission(self):
        return self._etatTransmission

    def _set_etatTransmission(self, nouvelEtat):

        # Pendant l'étape « transmissionDeclaration », le texte 3 est progressivement
        # mis à jour selon l'avancement
        if self.etape == 'transmissionDeclaration':
            self._etatTransmission = nouvelEtat
            if self.etatTransmission == 'Terminée':
                self.valTexte3.set(self.etatTransmission)
            else:
                self.valTexte3.set(self.etatTransmission + '...')
            self.texte3.update()

    etape = property(_get_etape, _set_etape)
    etatTransmission = property(_get_etatTransmission, _set_etatTransmission)

    def activerBtn3(self, evt):
        self.btn3.config(state=NORMAL)

    def selectionnerArchive(self):
        """ Ouverture du dialogue de sélection de fichier et affectation de l'archive sélectionnée au champ « champArchive » """
        archive = tkFileDialog.askopenfilename(initialdir=os.path.join(os.path.expanduser('~')), filetypes=[('Archives ZIP', '*.zip')], parent=self)

        self.champArchive.delete(0, len(self.champArchive.get()))
        self.champArchive.insert(0, archive)
        self.activerBtn3('')


    def afficherParametres(self):

        # Affichage de la fenêtre des paramètres
        fenParametres = FenParametres(self)
        fenParametres.transient(self)
        fenParametres.grab_set()
        self.wait_window(fenParametres)

    def centrerFenetre(self):
        """Méthode de positionnement de la fenêtre pour qu'elle soit centrée à l'écran"""

        self.update_idletasks()

        largeurFenetre, hauteurFenetre = self.winfo_reqwidth(), self.winfo_reqheight()
        largeurEcran, hauteurEcran = self.winfo_screenwidth(), self.winfo_screenheight()

        positionX = (largeurEcran - largeurFenetre) // 2
        positionY = (hauteurEcran - hauteurFenetre) // 2

        self.geometry("%dx%d%+d%+d" % (largeurFenetre, hauteurFenetre, positionX, positionY))
