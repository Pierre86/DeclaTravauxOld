#!/usr/bin/env python
# -*-coding:Utf-8 -*

from PyPDF2.pdf import PdfFileReader
import xml.etree.ElementTree as ET
import calendar
import os
import re

from DeclaTravaux.configuration import getParametres

REPERTOIRE_DECLARATIONS = getParametres()['Configuration']['RepertoireDeclarations']

def getListePdf(numeroDeclaration):
    """
    Retourne un dictionnaire associant le nom des fichiers PDF
    au nom des exploitants concernés.

    La fonction est relativement lourde dès lors qu'elle s'appuie sur la bibliothèque PyPDF2.
    """

    cheminDeclaration = REPERTOIRE_DECLARATIONS + '/' + numeroDeclaration

    # On se place dans le dossier donné en paramètre
    os.chdir(cheminDeclaration)
    # On liste les fichiers contenus dans le dossier
    listeFichiers = os.listdir()
    # La liste est filtrée pour ne conserver que les formulaires PDF
    listeFichiers = [fichier for fichier in listeFichiers if re.match(r'[0-9]{13}[A-Z]{1}([0-9][0-9])?_(.)*_[0-9]{1,2}.pdf', fichier)]
    # La liste est triée par ordre croissant
    listeFichiers.sort()

    # Un dictionnaire est créé sous la avec la structure suivante :
    # - clé = nom du fichier PDF ;
    # - valeur = nom de l'exploitant.

    listePdf = dict()

    for fichier in listeFichiers:
        champsPdf = PdfFileReader(fichier).getFields()
        nomExploitant = champsPdf['Exploitant']['/V']

        listePdf[fichier] = nomExploitant

    return listePdf

def getListeDestinataires(numeroDeclaration):
    """
    Retourne la liste des destinataires définis dans le fichier ***_description.xml,
    dont le chemin est indiqué en paramètre
    """

    cheminDeclaration = REPERTOIRE_DECLARATIONS + '/' + numeroDeclaration

    # On récupère le dictionnaire associant le nom de l'exploitant et son fichier PDF
    listePdf = getListePdf(numeroDeclaration)

    # On reconstitue le chemin du fichier « *_description.xml »
    cheminFichierXml = cheminDeclaration + '/' + numeroDeclaration + '_description.xml'

    # On parse le fichier XML et on se place à la racine
    fichierXml = ET.parse(cheminFichierXml)
    racine = fichierXml.getroot()

    # On indique le namespace utilisé dans le fichier XML
    ns = {'t': 'http://www.reseaux-et-canalisations.gouv.fr/schema-teleservice/2.2'}

    # On crée la liste des destinataires, qui contient l'ensemble des destinataires
    # indiqués dans le fichier XML sous la forme de dictionnaires,
    # qui contiennent un certain nombre d'informations.
    listeDestinataires = list()

    # On alimente la liste des destinataires.
    for tag in racine.findall('t:listeDesOuvrages/t:ouvrage', ns):

        ouvrage                 = tag.findtext('t:classeOuvrage', '', ns).replace('_', ' ')
        societe                 = tag.findtext('t:contact/t:societe', '', ns)
        agence                  = tag.findtext('t:contact/t:agence', '', ns)
        numero                  = tag.findtext('t:contact/t:numero', '', ns)
        voie                    = tag.findtext('t:contact/t:voie', '', ns)
        codePostal              = tag.findtext('t:contact/t:codePostal', '', ns)
        commune                 = tag.findtext('t:contact/t:commune', '', ns)
        codePays                = tag.findtext('t:contact/t:codePays', '', ns)
        courriel                = tag.findtext('t:contact/t:courriel', '', ns)
        telephone               = tag.findtext('t:contact/t:telephone', '', ns)
        fax                     = tag.findtext('t:contact/t:fax', '', ns)
        courrielUrgence         = tag.findtext('t:contact/t:mailUrgence', '', ns)
        telephoneUrgence        = tag.findtext('t:contact/t:telephoneUrgence', '', ns)
        faxUrgence              = tag.findtext('t:contact/t:faxUrgence', '', ns)
        telephoneEndommagement  = tag.findtext('t:contact/t:telEndommagement', '', ns)
        gestionFichiersDemat    = tag.findtext('t:contact/t:gestionDesFichiersDematerialises/t:gereLesFichiersDematerialises', '', ns)
        formatFichiersDemat     = tag.findtext('t:contact/t:gestionDesFichiersDematerialises/t:formatDesFichiersDematerialises', '', ns)

        # On associe le destinataire au fichier PDF qui lui correspond.
        fichierPdf = [nomFichier for nomFichier, nomExploitant in listePdf.items() if nomExploitant == societe][0]

        # On ajoute un dictionnaire à la liste avec l'ensemble des informations récupérées.
        listeDestinataires.append({ 'ouvrage':ouvrage,      'societe':societe,      'agence':agence,
                                    'numero':numero,        'voie':voie,            'codePostal':codePostal,
                                    'commune':commune,      'codePays':codePays,
                                    'courriel':courriel,    'telephone':telephone,  'fax':fax,
                                    'courrielUrgence':courrielUrgence,
                                    'telephoneUrgence':telephoneUrgence,
                                    'faxUrgence':faxUrgence,
                                    'telephoneEndommagement':telephoneEndommagement,
                                    'gestionFichiersDemat':gestionFichiersDemat,
                                    'formatFichiersDemat':formatFichiersDemat,
                                    'fichierPdf':fichierPdf
                                    })

    return listeDestinataires

def getIntituleDeclaration(numeroDeclaration):
    """
    Retourne l'intitulé complet de la déclaration dans un format lisible
    """

    #  (DT)
    #  (DICT)
    #
    #  (ATU)

    codesDeclarations = {   'DT': 'DT',
                            'DICT': 'DICT',
                            'DDC': 'DT-DICT conjointe',
                            'ATU': 'ATU'}

    typeDecla = codesDeclarations[numeroDeclaration.split('_')[1]]
    numero = numeroDeclaration.split('_')[0][8:].lstrip('0')
    jourDuMois = numeroDeclaration[6:8].lstrip('0')
    mois = calendar.month_name[int(numeroDeclaration[4:6])]
    annee = numeroDeclaration[0:4]
    jour = calendar.day_name[calendar.weekday(int(annee), int(numeroDeclaration[4:6]), int(jourDuMois))]


    intituleDeclaration = '{0} n° {1} du {2} {3} {4} {5}'.format(typeDecla, numero, jour, jourDuMois, mois, annee)

    return intituleDeclaration
