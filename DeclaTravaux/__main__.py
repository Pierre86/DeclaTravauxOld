#!/usr/bin/env python
# -*-coding:Utf-8 -*

# === DéclaTravaux ===
# Utilitaire de de télétransmission des déclarations d'intention de commencement de travaux (DICT),
# des déclarations de projet de travaux (DT) et des avis de travaux urgents (ATU) aux opérateurs concernés.

# Créé par Pierre GOBIN (contact@pierregobin.fr)

# Ce logiciel est régi par la licence CeCILL soumise au droit français et
# respectant les principes de diffusion des logiciels libres. Vous pouvez
# utiliser, modifier et/ou redistribuer ce programme sous les conditions
# de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
# sur le site « http://www.cecill.info ».

# En contrepartie de l'accessibilité au code source et des droits de copie,
# de modification et de redistribution accordés par cette licence, il n'est
# offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
# seule une responsabilité restreinte pèse sur l'auteur du programme,  le
# titulaire des droits patrimoniaux et les concédants successifs.

# À cet égard  l'attention de l'utilisateur est attirée sur les risques
# associés au chargement,  à l'utilisation,  à la modification et/ou au
# développement et à la reproduction du logiciel par l'utilisateur étant
# donné sa spécificité de logiciel libre, qui peut le rendre complexe à
# manipuler et qui le réserve donc à des développeurs et des professionnels
# avertis possédant  des  connaissances  informatiques approfondies.  Les
# utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
# logiciel à leurs besoins dans des conditions permettant d'assurer la
# sécurité de leurs systèmes et ou de leurs données et, plus généralement,
# à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

# Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
# pris connaissance de la licence CeCILL, et que vous en avez accepté les
# termes.


# Chargement des bibliothèques standards
import locale
import os
import re
import smtplib
import sys

# Chargement des bibliothèques locales

from DeclaTravaux.configuration import getParametres
from DeclaTravaux.interface_graphique import FenPrincipale

locale.setlocale(locale.LC_ALL, "")

# Définition des variables globales

PREMIER_LANCEMENT = getParametres()['Configuration'].getboolean('PremierLancement')

REPERTOIRE_RECHERCHE = getParametres()['Configuration']['RepertoireRecherche']
REPERTOIRE_DECLARATIONS = getParametres()['Configuration']['RepertoireDeclarations']

ADRESSE_ELECTRONIQUE = getParametres()['Courriels']['AdresseElectronique']
SERVEUR_SMTP = getParametres()['Courriels']['ServeurSMTP']
PORT_SERVEUR_SMTP = getParametres()['Courriels']['PortServeurSMTP']

def main():

    application = FenPrincipale()
    application.etape = 'demarrage'
    application.mainloop()

main()
