#!/usr/bin/env python
# -*-coding:Utf-8 -*

import os
import calendar
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from email.utils import formatdate

from DeclaTravaux.traitement_PDF import getIntituleDeclaration


def envoyerCourriel(serveur, adresseExpediteur, nomExpediteur, destinataire, repertoireDeclaration, numeroDeclaration):
    """
    Envoie par courriel les fichiers nécessaires au destinataire indiqué en paramètre.

    Les fichiers envoyés par courriel dépendent du niveau de dématérialisation du destinataire :
    - pas de dématérialisation : aucun courriel n'est envoyé ;
    - dématérialisation XML et PDF : envoi des fichiers « *_description.xml », « *_emprise.pdf » et « *_ZZZZ_*.pdf » ;
    - dématérialisation XML : envoi du fichier « *_description.xml ».
    """

    # Un courriel n'est envoyé que si le destinataire gère les fichiers dématérialisés
    if destinataire['gestionFichiersDemat']:

        adresseDestinataire = destinataire['courriel']

        # Le courriel envoyé contient un message, à la fois en version HTML et version texte, ainsi que des pièces jointes.
        #
        # Un tel courriel doit avoir la structure suivante :
        #   - Courriel (multipart/mixed)
        #       - Message (multipart/alternative)
        #           - Version texte du message (text/plain)
        #           - Version HTML du message (text/html)
        #       - Pièce jointe n° 1 (application/***)
        #       - ...
        #       - Pièce jointe n° n (application/***)


        # CRÉATION DE L'EN-TÊTE DU COURRIEL

        courriel = MIMEMultipart('mixed')

        courriel['From']    = adresseExpediteur
        courriel['To'] = adresseDestinataire
        courriel['Subject'] = 'Notification d’une déclaration de travaux'
        courriel['Date'] = formatdate(localtime=True)
        courriel['Charset'] = 'UTF-8'

        # CRÉATION DU MESSAGE (VERSION TEXTE ET VERSION HTML)

        partieMessage = MIMEMultipart('alternative')


        sousPartieTexte =   """
                            Madame, Monsieur,\n
                            Veuillez trouver ci-joint les éléments
                            relatifs à une déclaration de travaux.\n
                            Meilleures salutations,\n
                            """ + nomExpediteur

        sousPartieHtml =    """
                            <html>
                                <head></head>
                                <body>
                                    <p>Madame, Monsieur,</p>
                                    <p>
                                        Veuillez trouver ci-joint les éléments
                                        relatifs à une déclaration de travaux.
                                    </p>
                                    <p>Meilleures salutations,</p>
                                    <p>""" + nomExpediteur + """</p>
                                </body>
                            </html>
                            """

        # Attribution du type MIME correspondant à chaque version
        sousPartieTexte = MIMEText(sousPartieTexte, 'plain')
        sousPartieHtml = MIMEText(sousPartieHtml, 'html')

        # Leux deux versions sont intégrées au conteneur « Message »
        partieMessage.attach(sousPartieTexte)
        partieMessage.attach(sousPartieHtml)

        # Le conteneur Message est lui-même intégré au conteneur principal « Courriel »
        courriel.attach(partieMessage)


        # AJOUT DES PIÈCES JOINTES

        # Les pièces jointes sont listées dans la variable listePiecesJointes

        # Quelque soit le niveau de dématérialisation, le fichier *_description.xml est envoyé
        listePiecesJointes = [repertoireDeclaration + '/' + numeroDeclaration + '/' + numeroDeclaration + '_description.xml',]

        # Si la dématérialisation est de type XML / PDF, il faut ajouter les fichiers *_emprise.pdf et *_ZZZZ_*.pdf
        if destinataire['formatFichiersDemat'] == 'XML_PDF':
            # Fichier *_emprise.pdf
            listePiecesJointes.append(repertoireDeclaration + '/' + numeroDeclaration + '/' + numeroDeclaration + '_emprise.pdf')
            # Fichier *_ZZZZ_*.pdf
            listePiecesJointes.append(destinataire['fichierPdf'])

        # Chaque fichier contenu dans la variable listePiecesJointes
        # est intégré au conteneur principal « Courriel » en tant que pièce jointe.
        for fichier in listePiecesJointes:

            nomFichier = os.path.basename(fichier)
            pieceJointe = open(fichier, 'rb')

            partiePieceJointe = MIMEBase('application', 'octet-stream')
            partiePieceJointe.set_payload((pieceJointe).read())
            encoders.encode_base64(partiePieceJointe)
            partiePieceJointe.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(nomFichier))

            courriel.attach(partiePieceJointe)

        # Envoi du courriel
        courriel = courriel.as_string()
        #serveur.sendmail(adresseExpediteur, adresseDestinataire, courriel)

    # Dictionnaire de contrôle
    courrielEnvoye = {
                    'ouvrage':destinataire['ouvrage'],
                    'societe':destinataire['societe'],
                    'gestionDemat': destinataire['gestionFichiersDemat'],
                    'formatDemat':destinataire['formatFichiersDemat'],
                    'courriel':adresseDestinataire,
                    'fichiers':listePiecesJointes
                    }

    return courrielEnvoye

def envoyerCourrielRecap(serveur, expediteur, destinataire, numeroDeclaration, listeDesEnvois):
    """
    Envoie un courriel récapitulatif au destinataire indiqué en paramètre.

    """

    # Le courriel envoyé contient un message, à la fois en version HTML et version texte.
    # Il n'y a pas de pièce jointe.
    #
    # Un tel courriel doit avoir la structure suivante :
    #   - Courriel (multipart/alternative)
    #       - Version texte du message (text/plain)
    #       - Version HTML du message (text/html)

    # CRÉATION DE L'EN-TÊTE DU COURRIEL

    courriel = MIMEMultipart('alternative')

    courriel['From'] = expediteur
    courriel['To'] = destinataire
    courriel['Subject'] = 'Récapitulatif de la transmission électronique de la déclaration de travaux'
    courriel['Date'] = formatdate(localtime=True)
    courriel['Charset'] = 'UTF-8'

    # CRÉATION DU MESSAGE (VERSION TEXTE ET VERSION HTML)

    tableauTexte = ''
    tableauHtml = '<table style="border-collapse: separate; border-spacing: 0px 5px; margin: auto; font-size: small;"><tr><th>Destinataire</th><th>Courriel</th><th>Fichiers envoyés</th></tr>'

    for entree in listeDesEnvois:
        tableauTexte += '| ' + entree['societe'] + ' | ' + entree['courriel'] + ' | '
        tableauHtml += '<tr><td style="border-top: 1px solid black; border-bottom: 1px solid black; padding: 4px 8px;"><class style="font-weight: bold">' + entree['societe'] + '</class><br />' + entree['ouvrage'] +'</td>'
        tableauHtml += '<td style="border-top: 1px solid black; border-bottom: 1px solid black; padding: 4px 8px;">' + entree['courriel'] + '</td>'
        tableauHtml += '<td style="border-top: 1px solid black; border-bottom: 1px solid black; padding: 4px 8px;">'

        for fichier in entree['fichiers']:
            tableauTexte += os.path.basename(fichier) + ' '
            tableauHtml += os.path.basename(fichier) + '<br />'

        tableauTexte += ' |' + '\n'
        tableauHtml += '</td></tr>'

    tableauHtml += '</table>'

    partieTexte = ''
    partieTexte += 'Voici le récapitulatif de transmission de la déclaration :\n\n'
    partieTexte += getIntituleDeclaration(numeroDeclaration) + '\n\n'
    partieTexte += tableauTexte


    partieHtml = """
                    <html>
                        <head></head>
                        <body>
                            <p>Voici le récapitulatif de transmission de la déclaration :</p>
                            <p style="text-align: center; font-weight: bold;">
                            """ + getIntituleDeclaration(numeroDeclaration) + """
                            </p>
                            """ + tableauHtml + """
                        </body>
                    </html>
                    """

    # Attribution du type MIME correspondant à chaque version
    partieTexte = MIMEText(partieTexte, 'plain')
    partieHtml = MIMEText(partieHtml, 'html')

    # Leux deux versions sont intégrées au courriel
    courriel.attach(partieTexte)
    courriel.attach(partieHtml)

    # Envoi du courriel
    courriel = courriel.as_string()
    serveur.sendmail(expediteur, destinataire, courriel)

