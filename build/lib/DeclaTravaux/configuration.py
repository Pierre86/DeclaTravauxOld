#!/usr/bin/env python
# -*-coding:Utf-8 -*

import configparser
import platform
import os
import keyring

def getCheminFichierConfig():
    """
    Retourne le chemin du fichier de configuration de l'application
    """
    cheminFichier = ''

    # Le chemin du fichier de configuration dépend du système d'exploitation
    if platform.system() == 'Linux':

        try:
            repConfig = os.environ['XDG_CONFIG_HOME']
        except KeyError:
            repConfig = os.path.join(os.path.expanduser('~'), '.config')
        finally:
            cheminFichier = os.path.join(repConfig, 'declaTravaux', 'declaTravaux.conf')

    elif platform.system() == 'Windows':
        cheminFichier = os.path.join(os.environ['AppData'], 'DeclaTravaux', 'DeclaTravaux.conf')

    return cheminFichier


def creerFichierConfig(cheminFichier=''):
    """
    Fonction créant le fichier de configuration par défaut
    """

    config = configparser.ConfigParser()
    config.optionxform = str

    repDeclarations = os.path.join(os.path.expanduser('~'), 'Documents', 'DéclaTravaux')
    
    # Par défaut, les archives sont recherchées dans le répertoire « Téléchargements »
    # Si ce dernier n'est pas trouvé, elles sont recherchées dans le dossier de l'utilisateur
    if platform.system() == 'Linux':
        
        try:
            repRecherches = os.environ['XDG_DOWNLOAD_DIR']
        
        except KeyError:
            repRecherches = os.path.join(os.path.expanduser('~'), 'Téléchargements')
            
            if not os.path.isdir(repRecherches):
                repRecherches = os.path.expanduser('~')
    
    elif platform.system() == 'Windows':
        repRecherches = os.path.join(os.path.expanduser('~'), 'Downloads')
            

    config['Configuration'] = {'PremierLancement': "1",
                                'RepertoireDeclarations': repDeclarations,
                                'RepertoireRecherche': repRecherches}

    config['Courriels'] = { 'AdresseElectronique': '',
                            'ServeurSMTP': '',
                            'PortServeurSMTP': '',
                            'NomExpediteur': ''}


    os.makedirs(os.path.dirname(cheminFichier), exist_ok=True)

    with open(cheminFichier, 'w') as fichierConfig:
        config.write(fichierConfig)


def getParametres():
    """
    Fonction retournant les paramètres indiqués dans le fichier de configuration
    """

    config = configparser.ConfigParser()
    config.optionxform=str

    # Le chemin du fichier de configuration dépend du système d'exploitation
    cheminFichier = getCheminFichierConfig()

    # Si le fichier de configuration n'existe pas, on le crée
    if not os.path.isfile(cheminFichier):
        creerFichierConfig(cheminFichier)

    # On charge les informations
    config.read(cheminFichier)

    return config

def setParametres(**kwargs):
    """
    Fonction modifiant les paramètres contenus dans le fichier de configuration
    """

    config = getParametres()
    cheminFichier = getCheminFichierConfig()

    # Le paramètre PremierLancement est automatiquement affecté de la valeur 0
    config['Configuration']['PremierLancement'] = '0'

    # On boucle les paramètres donnés en arguments
    # S'ils correspondent aux paramètres attendus, on modifie les paramètres
    for parametre, valeur in kwargs.items():

        if parametre in ('RepertoireDeclarations', 'RepertoireRecherche'):

            config['Configuration'][parametre] = valeur

        elif parametre in ( 'AdresseElectronique',
                            'ServeurSMTP', 'PortServeurSMTP', 'NomExpediteur'):

            config['Courriels'][parametre] = valeur

    # Le fichier de configuration est sauvegardé
    with open(cheminFichier, 'w') as fichierConfig:
        config.write(fichierConfig)

def getMotDePasse():
    motDePasse = keyring.get_password('DéclaTravaux', os.environ['USER'])

    if not motDePasse:
        motDePasse = ''

    return motDePasse

def setMotDePasse(motDePasse):
    keyring.set_password('DéclaTravaux', os.environ['USER'], motDePasse)
